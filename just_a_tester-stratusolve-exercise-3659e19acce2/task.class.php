<?php
/**
 * This class handles the modification of a task object
 */
class Task {
    public $TaskId;
    public $TaskName;
    public $TaskDescription;
    protected $TaskDataSource;
    public function __construct($Id = null) {
        $this->TaskDataSource = file_get_contents('Task_Data.txt');
        if (strlen($this->TaskDataSource) > 0)
            $this->TaskDataSource = json_decode($this->TaskDataSource); // Should decode to an array of Task objects
        else
            $this->TaskDataSource = array(); // If it does not, then the data source is assumed to be empty and we create an empty array

        if (!$this->TaskDataSource)
            $this->TaskDataSource = array(); // If it does not, then the data source is assumed to be empty and we create an empty array
        if (!$this->LoadFromId($Id))
            $this->Create();
    }
    protected function Create() {
        // This function needs to generate a new unique ID for the task
        // Assignment: Generate unique id for the new task
        $this->TaskId = $this->getUniqueId();
        $this->TaskName = 'New Task';
        $this->TaskDescription = 'New Description';
    }
    protected function getUniqueId() {
        // Assignment: Code to get new unique ID
		$this->TaskDataSource = file_get_contents('Task_Data.txt');
        	if (strlen($this->TaskDataSource) > 0){
				$this->TaskDataSource = json_decode($this->TaskDataSource);
				return count($this->TaskDataSource) + 1;
			  }else{
				return 1;
			  }
    }
    protected function LoadFromId($Id = null) {
        if ($Id) {
            // Assignment: Code to load details here...

        } else
            return null;
    }

    public function Save($saveObj) {
        //Assignment: Code to save task here
		$this->TaskDataSource = file_get_contents('Task_Data.txt');
		$TaskSource = $this->TaskDataSource;
		$result = json_decode($saveObj, true);
		$jsonString = json_decode($TaskSource, true);
		$exists = "false";
		$returnValue = "Default";
		foreach ($jsonString as $value) 
		{	
			
		 	if( $value['TaskId'] == $result['TaskId']){

						$exists = "true";
						break;	
		    }
		}
		
		if($exists == "false"){
					$result['TaskId'] = $this->getUniqueId();
					array_push($jsonString, $result);
					$myFile = 'Task_Data.txt';
					$handle = fopen($myFile, 'w') or die('Cant open'.$myFile);
					$data = stripslashes(json_encode($jsonString, true));
					fwrite($handle, $data);
					$returnValue = "Item Added";
					
		}
		if($exists == "true"){
			foreach ($jsonString as $key => $value) 
		{	
		 	if( $value['TaskId'] == $result['TaskId']){
				if($result['TaskName'] == "" or $result['TaskDescription'] == ""){
					$returnValue = "Please enter a value in both text boxes";
				}else{
						$jsonString[$key]['TaskName'] = $result['TaskName'];
						$jsonString[$key]['TaskDescription'] = $result['TaskDescription'];
						$myFile = 'Task_Data.txt';
						$handle = fopen($myFile, 'w') or die('Cant open'.$myFile);
						$data = json_encode($jsonString, true);
						fwrite($handle, $data);
						$returnValue = "Item updated";
				}
						
		    }
			 
						
		}
			
		}
		return $returnValue;
  }
    
    public function Delete($saveObj) {
        //Assignment: Code to delete task here
		$this->TaskDataSource = file_get_contents('Task_Data.txt');
		$TaskSource = $this->TaskDataSource;
	 
	$returnValue = "Default";
	$dCodeContents = json_decode($TaskSource, true) ;
   
	$result = json_decode($saveObj, true) ;

		foreach ($dCodeContents as $key => $value) 
		{
			
			if( $value['TaskId'] == $result['TaskId'])
			{	
				array_splice($dCodeContents, $key, 1);
						
			}
						
		}
		$myFile = 'Task_Data.txt';
				$handle = fopen($myFile, 'w') or die('Cant open'.$myFile);
				$dCodeContents = json_encode($dCodeContents, true);
				fwrite($handle, $dCodeContents);
				
	$returnValue = "Item Deleted";
	return $returnValue;
  }
    }

?>