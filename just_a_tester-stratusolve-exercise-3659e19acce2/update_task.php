<?php
/**
 * This script is to be used to receive a POST with the object information and then either updates, creates or deletes the task object
 */
require('Task.class.php');
// Assignment: Implement this script

$task = new task();

 if(isset($_POST['save'])) {
    $json = $_POST['save'];
	$result = $task->Save($json);
	echo $result;
  
 }
 if(isset($_POST['delete'])) {
	$json = $_POST['delete'];
	$result = $task->Delete($json);
	echo $result;
  }
 
 
  ?>
